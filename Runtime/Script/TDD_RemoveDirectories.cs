﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TDD_RemoveDirectories : MonoBehaviour
{
    public string m_location;
    public string m_currentPath;
    public int m_removed;
    public int m_scanned;
    public string[] ignore = new string[] {"Temp",
"Obj","Build","UnityGenerated", "Library" };

    void Start()
    {
        RemoveEmptyDirectories(m_location);
    }

    private void RemoveEmptyDirectories(string path)
    {
        m_currentPath = path;
        foreach (var directory in Directory.GetDirectories(path))
        {
            if (!Containt(directory, ignore))
            {
                m_scanned++;
                RemoveEmptyDirectories(directory);
                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    m_removed++;
                    Directory.Delete(directory, false);
                }
            }
        }
    }

    private bool Containt(string directory, string[] ignore)
    {
        for (int i = 0; i < ignore.Length; i++)
        {
            if (directory.ToLower().IndexOf(ignore[i].ToLower()) > -1)
                return true;
        }
        return false;
    }
}
